package com.devsutd.pocsfmc;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.salesforce.marketingcloud.*;
import com.salesforce.marketingcloud.registration.RegistrationManager;

import org.json.JSONObject;

/**
 * Created by rvanoni on 10/23/2017.
 */

public class AndroidApp extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp firebaseApp = FirebaseApp.initializeApp(this);
        MultiDex.install(this);

        MarketingCloudSdk.init(this, MarketingCloudConfig.builder()
                .setApplicationId("7c6ed5de-7236-4d64-8b41-404a3f7dbded")
                .setAccessToken("g3pahc2c8x44xfmz4f5563c9")
                .setGcmSenderId("AAAAeWHelbc:APA91bE6q2L2nezCQouwDKVvyk4tuGcv7PdWangHAPVCw16dMhzZwtwuSv76W-C0pmdHWC6tnGdkp3e8Uydx-15Ylc5CssiUJBQrIO7N8cTuX5j2SMXlGUI-a2H_wJiYnsuRmLz3Ww_Z")
                //Enable any other feature desired.
                .build(),
                new MarketingCloudSdk.InitializationListener() {
                    @Override
                    public void complete(InitializationStatus status) {
                        if (status.isUsable()) {
                            if (status.status() == InitializationStatus.Status.COMPLETED_WITH_DEGRADED_FUNCTIONALITY) {
                                // While the SDK is usable, something happened during init that you should address.
                                // This could include:
                                if (status.locationsError()) {
                                //Google play services encountered a recoverable error
                               /* GoogleApiAvailability.getInstance()
                                    .showErrorNotification(AndroidApp.this, status.locationPlayServicesStatus());
                                */}
                                else if(status.messagingPermissionError()) {
                                    /* The user had previously provided the location permission, but it has now been revoked.
                                     Geofence and Beacon messages have been disabled.  You will need to request the location
                                     permission again and re-enable Geofence and/or Beacon messaging again. */
                                }
                            } else
                                MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
                                    @Override
                                    public void ready(MarketingCloudSdk marketingCloudSdk) {
                                        RegistrationManager registrationManager = marketingCloudSdk.getRegistrationManager();

                                        //Set contact key
                                        registrationManager
                                                .edit()
                                                .setContactKey("rvanoni@devsutd.com")
                                                .commit();

                                        String token = marketingCloudSdk.getPushMessageManager().getPushToken();

                                        if (token == null || token == "") {

                                            token = FirebaseInstanceId.getInstance().getToken();
                                            marketingCloudSdk.getPushMessageManager().setPushToken(token);
                                        }
                                    }
                                });
                        } else {
                            //Something went wrong with init that makes the SDK unusable.
                        }
                    }
                });
    }
}
