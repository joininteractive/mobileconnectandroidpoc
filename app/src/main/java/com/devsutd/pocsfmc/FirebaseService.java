package com.devsutd.pocsfmc;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.salesforce.marketingcloud.MarketingCloudSdk;

import org.json.JSONObject;

/**
 * Created by rvanoni on 11/1/2017.
 */

public class FirebaseService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        MarketingCloudSdk.getInstance().getPushMessageManager().setPushToken(refreshedToken);
        JSONObject sdk = MarketingCloudSdk.getInstance().getSdkState();
    }
}
